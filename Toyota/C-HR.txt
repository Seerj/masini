he Toyota C-HR is a subcompact crossover SUV produced by Toyota.
 The production version of the C-HR was unveiled at the March 2016 Geneva Motor Show.
  The North American production version was also unveiled at the November 2016 LA Auto Show. The production of the C-HR
   started in November 2016. It was launched in Japan on 14 December 2016, and in Europe, Australia and North America 
   in early 2017. It succeeds the XA30 RAV4 in Japan. The name C-HR stands for Compact High Rider,
 Cross Hatch Run–about or Coupé High–Rider.